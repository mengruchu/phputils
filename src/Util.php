<?php

namespace tycloud\util;

class Util
{
    /**
     * 小数位格式(不四舍五入)
     *
     * @param $value
     * @param $format
     * @return string
     */
    public function decimalFormat($value, $format)
    {
        $next = $format + 1;
        return sprintf("%.{$format}f", substr(sprintf("%.{$next}f", $value), 0, -$format));
    }
}