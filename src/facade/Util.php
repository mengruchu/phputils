<?php

namespace tycloud\util\facade;

use think\Facade;

/**
 * Class Util
 *
 * @package thans\jwt\facade
 * @mixin \tycloud\util\Util
 * @method string decimalFormat($value, $format) static 小数位格式(不四舍五入)
 */
class Util extends Facade
{
    protected static function getFacadeClass()
    {
        return 'tycloud\util\Util';
    }
}
